<?php
//Searching providers

session_start();

include 'lib/connect_db.php';
include 'zipcode_functions.php';

$userID = $_SESSION['userID']; 

$keyword = $_POST['keyword'];
$skill = $_POST['skill'];
$range = $_POST['range'];

$keyword = mysqli_real_escape_string($connect, $keyword);
$skill = mysqli_real_escape_string($connect, $skill);
$range = mysqli_real_escape_string($connect, $range);


if($range=="< 1 miles"){
	$range=1;
}
else if($range=="< 10 miles"){
	$range=10;
}
else if($range=="< 30 miles"){
	$range=30;
}
else{
	$range="any";
}


//Find the user's zipcode. I will activate this after search is connected to login page.
/*$sql= "select Zipcode from profiles where UserName='$_SESSION['ses_username']'; ";
$res = mysqli_query($connect,$sql);
$array = mysqli_fetch_array($res);
$zipcode = $array[Zipcode];
*/



if($skill=="any"){
	if($keyword=="" && $range=="any"){
		echo "<script>
			alert('You should choose at least one option');
			location.replace('search_employer.html'); 
			</script>";
	}
	else if($range=="any"){
		$range=30;
		$arr=getZipCodesInRadius(32723, $range);
		$sql = "select UserName, DOB, Bio, Skills, Zipcode from profiles where Bio like '%{$keyword}%' and (Zipcode='$arr[0]' or Zipcode='$arr[1]' or Zipcode='$arr[2]' or Zipcode='$arr[3]' or Zipcode='$arr[4]'); ";
		$res = mysqli_query($connect,$sql);
	}
	else{
		$arr=getZipCodesInRadius(32723, $range);
		$sql = "select UserName, DOB, Bio, Skills, Zipcode from profiles where Bio like '%{$keyword}%' and (Zipcode='$arr[0]' or Zipcode='$arr[1]' or Zipcode='$arr[2]' or Zipcode='$arr[3]' or Zipcode='$arr[4]'); ";
		$res = mysqli_query($connect,$sql);
	}
}
	
else{
	$sql = "select SKILLID from skills where Name='$skill'; ";
	$res = mysqli_query($connect,$sql);
	$array = mysqli_fetch_array($res);
	$skill = $array[SKILLID];

	if($range=="any"){
		$range=30;
		$arr=getZipCodesInRadius(32723, $range);
		$sql = "select UserName, DOB, Bio, Skills, Zipcode from profiles where Bio like '%{$keyword}%' and Skills='$skill' and (Zipcode='$arr[0]' or Zipcode='$arr[1]' or Zipcode='$arr[2]' or Zipcode='$arr[3]' or Zipcode='$arr[4]'); ";
		$res = mysqli_query($connect,$sql);
	}
	else{
		$arr=getZipCodesInRadius(32723, $range);
		$sql = "select UserName, DOB, Bio, Skills, Zipcode from profiles where Bio like '%{$keyword}%' and Skills='$skill' and (Zipcode='$arr[0]' or Zipcode='$arr[1]' or Zipcode='$arr[2]' or Zipcode='$arr[3]' or Zipcode='$arr[4]'); ";
		$res = mysqli_query($connect,$sql);
	}
}

$username_array = array();	
$skills_array = array();
$zipcode_array = array(); 

while($array = mysqli_fetch_array($res)){
	$sql="select Name from skills where SKILLID='$array[Skills]';";
	$result = mysqli_query($connect,$sql);
	$arr = mysqli_fetch_array($result);
	$skill = $arr['Name'];
	
	$username_array[] = $array['UserName'];
	$skills_array[] = $skill;
	$zipcode_array[] = $array['Zipcode'];
}

	$return_array = array($username_array, $skills_array, $zipcode_array);
	
	
	echo json_encode($return_array);

?>
