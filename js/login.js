
/*
	The purpose of this file is to validate the user's submission for the signup.html page, and then call the proper php script which will either:

	1. Create an account
	2. Return a flag that says the email/username already exists 

	@author Jacob Hell
*/

$(document).ready(function(){
	$("#myForm").validate({
	/*
		Validate function will check every input in the form based on what constraints are placed.

		For more information on the constraints: http://jqueryvalidation.org/validate
	*/
			rules: {
				username: {
					required: true,
				},

				password: {
					required: true,
				},

			},
			/*
				What error message will display if the form fails in a certain scenario
			*/ 

			messages :{
				username: {
					required: "Username required"
				},
				
				password: {
					required: "Password required",
				},
			},
		/*
			submitHandler is a callback which handles the actual submit when the form is submitted. For
			our purposes, we call the $.post function which sends data to the php script, and in turn
			creates an account, or returns a flag saying that the username/email exists.

			If there is an error on the backend, an alert will display that says "error".
		*/ 		

		
		submitHandler: function() {	
			$.post("login_script.php",
				{
					username: $("#username").val(),
					password: $("#password").val(),	
				}, function(data) {
					var status = data;
					
				if(status === "true1")
				{	
					window.location.href = "admin.html";
				}
				
				else if(status === "true2"){
					
					alert("You are blacklisted");
					
				}
				else if(status === "true")
				{	
					window.location.href = "profile.html";
				}

				else 
				{	
					alert("Invalid username/password");
				}

				})				
					
				.fail(function(xhr, textStatus, errorThrown) {
					alert("error");
	        		console.log(xhr.responseText);
				});
			}
	
	});		

});
