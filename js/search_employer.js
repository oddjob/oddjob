/*
	The purpose of this file is to display an html table that holds the results of the search

	@author Jacob Hell
*/
var json_array;
$(document).ready(function(){
	
	// the purpose of this post function is to send data to the php search file 
	$("#myForm").submit(function(e){


		e.preventDefault();


		//$(".results").css('visibility', 'hidden');
		//$('#table').find('tbody').remove();



		$.post('searchWorker.php',
		{
			keyword: $("#keyword").val(),
			range: $("#range").val(),
			skill: $("#skill").val()
		}, function(data){
				json_array = data;
		}, "json")

		.fail(function(xhr, textStatus, errorThrown){
			alert("error");
		})
		.success(function(){
			$('#table tr:not(:first)').remove();
			$('.page').remove();
			//Paging unit
			var rows = json_array[0].length;   //the number of <tr>s
			var per_page = 7;
			var no_pages = Math.ceil(rows / per_page);    // the number of pages
			var pageNumbers = $("<br><div id='pages'></div>");
			for(var i = 0; i <no_pages; i++) {
			$("<span class='page'>"+(i+1) +"</span>").appendTo(pageNumbers);
			}	
			$("#table").after(pageNumbers);	

			
			$(".page").hover(function() {  //<span class='page'>"+(i+1) +"</span>
				$(this).addClass("hov");
			}, function() {
				$(this).removeClass("hov");
			}); 
			
			//initial table
			for(var i =0; i < per_page; i++){
				if(rows<=i){
					break;
				}
				$('#table tr:last').after("<tr><td>" + json_array[0][i] + "</td><td>" + json_array[1][i] + "</td><td>" + json_array[2][i] + "</td><td>" + 
				json_array[3][i] + "</td></tr>");
			}
		
			//when clicking page number 
			$("span").click(function(event){
				$('#table tr:not(:first)').remove();
				for(var i = ($(this).text() -1 ) * per_page; i <= $(this).text() * per_page -1 ; i++){
					if(rows<=i){
						break;
					}
					$('#table tr:last').after("<tr><td>" + json_array[0][i] + "</td><td>" + json_array[1][i] + "</td><td>" + json_array[2][i] + "</td><td>" + 
					json_array[3][i] + "</td></tr>");
				}
				//hovering <tr>s
				$("tr").hover(function() {
					$(this).find("td").addClass("hov");
				}, function() {
					$(this).find("td").removeClass("hov");
				});
			});	

			//hovering <tr>s
			$("tr").hover(function() {
				$(this).find("td").addClass("hov");
			}, function() {
				$(this).find("td").removeClass("hov");
			});
			
		});

//The following two lines will add the search results to the table, and make the table visible

		$(".results").css('visibility', 'visible');

	});
});
