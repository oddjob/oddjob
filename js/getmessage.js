$(document).ready(function(){
	//posted job update
	$.post("get_message_script.php", function(data){
		if(data.length === 0)
		{
			$("#get_message").append('<option value="none" disabled="disabled">No Messages Currently Received</option>');
		}
		else {
			json_array = data;
		}
	},"json")
	.fail(function(xhr, textStatus, errorThrown){
			alert("error");
		})
	.success(function(){
		for(var i =0; i < json_array[0].length; i++){
				$("#get_message").append('<option value = ' + json_array[0][i] + '>' + '[' + json_array[1][i] + '] : ' + json_array[2][i] + '</option>');
		}
	});

	$('#get_message').on('dblclick', 'option', function() {
		var text;
		var messageID = $( "select option:selected" ).val();
		$.post("get_message_text_script.php", 
		{
			id: $('#get_message').val()
		}, function(data){
			msgType = data.a; 
			text = data.b;
		}, "json")
		
		.fail(function(){
			alert("server error");
		})
		.success(function(){
			if(msgType === 0)
			{
    			$("#dialog").html(text);
				$("#dialog").dialog({
				resizeable: false,
				modal: true,
				buttons: {
					"Send Job Request": function() {
						$(this).dialog("close");
					},
				}
			});
			}
			else if(msgType === 1)
			{
				$("#dialog").html(text);
				$("#dialog").dialog({
				resizeable: false,
				modal: true,
				buttons: {
					"Accept Job": function() {
						//
						$.post("send_accept_request_message.php",
						{
							id:messageID
						}, function(data){
							alert("Message Sent!");
						})
						.fail(function(){
							alert("server error");
						});
						$(this).dialog("close");
						//
					},
					"Decline Job": function() {
						//
						$.post("send_decline_request_message.php",
						{
							id:messageID
						}, function(data){
							alert("Message Sent!");
						})
						.fail(function(){
							alert("server error");
						});
						$(this).dialog("close");
						//
					}
				}

			});
			}	
		});
	});

});
