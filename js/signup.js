/*
	The purpose of this file is to validate the user's submission for the signup.html page, and then call the proper php script which will either:

	1. Create an account
	2. Return a flag that says the email/username already exists 

	@author Jacob Hell
*/


$(document).ready(function(){
	/*
		Validate function will check every input in the form based on what constraints are placed.

		For more information on the constraints: http://jqueryvalidation.org/validate
	*/
	$("#myForm").validate({
			rules: {
				username: {
					required: true,
					minlength: 3,
					maxlength: 15
				},

				email: {
					required: true,
					email: true,
					maxlength: 50
				},

				reemail: {
					required: true,
					equalTo: "#email"
				},

				password: {
					required: true,
					minlength: 10,
					pattern: /^(?=.*[A-Z])(?=.*\d).*$/
				},

				repassword: {
					required: true,
					equalTo: "#password"
				}

			},
			/*
				What error message will display if the form fails in a certain scenario
			*/ 
			messages :{
				username: {
					required: "Username required"
				},
				
				email: {
					required: "Email required"
				},
				
				reemail : {
					equalTo: "Doesn't match email"
				},
				
				password: {
					required: "Password required",
					pattern: "Password must have one uppercase letter and one number"
				},
				
				repassword: {
					equalTo: "Doesn't match password"
				}
			},
		/*
			submitHandler is a callback which handles the actual submit when the form is submitted. For
			our purposes, we call the $.post function which sends data to the php script, and in turn
			creates an account, or returns a flag saying that the username/email exists.

			If there is an error on the backend, an alert will display that says "error".
		*/ 		

			submitHandler: function() {	
			$.post("signup_script.php",
				{
					username: $("#username").val(),
					password: $("#password").val(),
					email: $("#email").val()	
				}, function(data) {
					
					var status = data.replace(/[^a-zA-Z]/g,' ');

					alert(status);
				})				
					
				.fail(function(xhr, textStatus, errorThrown) {
					alert("error");
	        		console.log(xhr.responseText);
				});
			}
	
	});		

});
