<?php

include_once("query.php");

// Username of user	String
// Password of user	Password String (unhashed, hash here)
// Email of user	String
function createUser($UserName, $pass, $email){
	if(emailExists($email) || usernameExists($UserName)){
		return false;
	}
       	$hashed_password = password_hash($pass, PASSWORD_DEFAULT);
		$sql = "insert into profiles (UserName, Password, Email) values ('$UserName', '$hashed_password', '$email'); ";

	        $res = query($sql);
		return true; 
	
}


function deleteUser($USERID){
	echo "Entered deleteUser\n";
	$sql = "delete from profiles where USERID = '$USERID'; ";
        $res = query($sql);
	if($res == false){
		echo "User Delete Failed\n";
	}
	
}

function getUSERID($Username){
	$sql = "select USERID from profiles where UserName = '$Username'; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['USERID'];
}

function getAdminUSERIDs(){
	$sql = "select USERID from profiles where IsAdmin = true ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['USERID'];
}

function usernameExists($username){
	$sql = "select count(*) from profiles where UserName='$username'";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	
	if($rs[0]>0){
		//This doesn't print out. It just returns false
		return true;
	}
	else{
		//This doesn't print out. It just returns true
		return false;
	}	
}

function emailExists($email){
	$sql = "select count(*) from profiles where email='$email'";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	
	if($rs[0]>0){
		//This doesn't print out. It just returns false
		return true;
	}
	else{
		//This doesn't print out. It just returns true
		return false;
	}	
}


function setUserName($USERID, $name){
	$sql = "update profiles set UserName = '$name' where USERID = '$USERID'; "; 
        $res = query($sql);
}

function getUserName($USERID){
	$sql = "select UserName from profiles where USERID = '$USERID'; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['UserName'];
}

function setPassword($USERID, $pass){
	$hashed_password = password_hash($pass, PASSWORD_DEFAULT);
	$sql = "insert into profiles (Password) values ($hashed_password); ";
        $res = query($sql);
}

function getPassword($USERID){
	$sql = "select Password from profiles where USERID = '$USERID'; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['Password'];

}

function setEmail($USERID, $email){
	$sql = "update profiles set Email = '$email' where USERID = '$USERID'; ";
        $res = query($sql);
}

function getEmail($USERID){
	$sql = "select Email from profiles where USERID = '$USERID'; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['Email'];
}

function setDOB($USERID, $dob){
	$sql = "update profiles set DOB = '$dob' where USERID = '$USERID'; ";
        $res = query($sql);
}

function getDOB($USERID){
	$sql = "select DOB from profiles where USERID = '$USERID'; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['DOB'];
}

function setJoinDate($USERID, $date){
	$sql = "update profiles set JoinDate = '$date' where USERID = '$USERID'; ";
        $res = query($sql);
}

function getJoinDate($USERID){
	$sql = "select JoinDate from profiles where USERID = '$USERID'; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['JoinDate'];
}

function setBio($USERID, $bio){
	$sql = "update profiles set Bio = '$bio' where USERID = '$USERID'; ";
        $res = query($sql);
}

function getBio($USERID){
	$sql = "select Bio from profiles where USERID = '$USERID'; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['Bio'];
}

function setPhone($USERID, $phone){
	$sql = "update profiles set Phone = '$phone' where USERID = '$USERID'; ";
        $res = query($sql);
}

function getPhone($USERID){
	$sql = "select Phone from profiles where USERID = '$USERID'; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['Phone'];
}

function setUserRating($USERID, $rating){
	$sql = "update profiles set UserRating = '$rating' where USERID = '$USERID'; ";
        $res = query($sql);
}

function getUserRating($USERID){
	$sql = "select UserRating from profiles where USERID = '$USERID'; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['UserRating'];
}

function setUserSkills($USERID, $skills){
	$sql = "update profiles set Skills = '$skills' where USERID = '$USERID'; ";
        $res = query($sql);
}

function getUserSkills($USERID){
	$sql = "select Skills from profiles where USERID = '$USERID'; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['Skills'];
}

function setHasVenmo($USERID, $bool){
	$sql = "update profiles set HasVenmo = '$bool' where USERID = '$USERID'; ";
        $res = query($sql);
}

function getHasVenmo($USERID){
	$sql = "select HasVenmo from profiles where USERID = '$USERID'; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['HasVenmo'];
}

function setLoginDate($USERID, $date){
	$sql = "update profiles set LoginDate = '$date' where USERID = '$USERID'; ";
        $res = query($sql);
}

function getLoginDate($USERID){
	$sql = "select LoginDate from profiles where USERID = '$USERID'; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['LoginDate'];
}

function setUserLocation($USERID, $zipcode){
	$sql = "update profiles set Location = '$zipcode' where USERID = '$USERID'; ";
        $res = query($sql);
}

function getUserLocation($USERID){
	$sql = "select Location from profiles where USERID = '$USERID'; ";
	$res = query($sql);
	if(empty($res))
	{
		return "undefined";
	}
	$rs = mysqli_fetch_array($res);
	return $rs['Location'];
}

function setHashedPassword($USERID, $pass){
	$hashed_password = password_hash($pass, PASSWORD_DEFAULT);
	$sql = "update profiles set Password = '$hashed_password' where USERID = '$USERID'; ";
        $res = query($sql);
}

function confirmPassword($USERID, $pass){
	$sql = "select Password from profiles where USERID = '$USERID'; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	$temp = $rs['Password'];
	$bool = password_verify($pass, $temp);
	return $bool;
}

function confirmUsername($USERID, $user){
	$sql = "select count(*) from profiles where UserName = '$user' ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	$num = $rs[0];
	if($num>0){
		 //echo "<script> alert('Username is already being used. Choose another Username')  history.back()</script>";
		return true; 	
		//die; //stop program
	}
	else{
		return false; 
	}
}

function confirmEmail($USERID, $email){
	$sql = "select count(*) from profiles where Email = '$email' ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	$num = $rs[0];
	if($num>0){
		echo "<script>()
			alert('Email is already being used.');
			history.back();
			</script>";
			die; //stop program
 	}
}

//Returns the length of the profiles table
function sizeOfProfiles(){
	$sql = "Select count (*) from profiles; ";
        $res = query($sql);
	$rs = mysqli_fetch_row($res);
	return $rs[0];
}

function checkIfInfoIsNull($USERID){
	$sql1 = "SELECT Zipcode,Bio FROM profiles WHERE USERID = '$USERID' 
	AND Zipcode IS NULL";

	$res1 = query($sql1);
	$rs1 = mysqli_fetch_array($res1);
	$num1 = $rs1[0];
	if($num1>0){
		return true;
	}
	else {

		$sql2 = "SELECT Zipcode,Bio FROM profiles WHERE USERID = '$USERID' 
	AND Bio IS NULL";

		$res2 = query($sql2);
		$rs12 = mysqli_fetch_array($res2);
		$num2 = $rs2[0];

		if($num1>0){
			return true;
	}

		else {
			return false; 
		}

	}
}

//receives true or false for bool
function setIsAdmin($USERID, $bool){
        $sql = "update profiles set IsAdmin = $bool where USERID = $USERID; ";
        $res = query($sql);
}

//returns 0  for false, 1 for true
function getIsAdmin($USERID){
        $sql = "select IsAdmin from profiles where USERID = $USERID; ";
        
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
        return $rs['IsAdmin'];
}

//receives true or false for bool
function setIsBlacklisted($USERID, $bool){
        $sql = "update profiles set IsBlacklisted = $bool where USERID = $USERID; ";
	$res = query($sql);
}

//returns 0  for false, 1 for true
function getIsBlacklisted($USERID){
        $sql = "select IsBlacklisted from profiles where USERID = $USERID; ";
        
	$res = query($sql);
        $rs = mysqli_fetch_array($res);
        return $rs['IsBlacklisted'];
}

?>
