<?php
	//used for displaying the proper message in the inbox page
	include_once("messages_Functions.php");
	include_once("profiles_Functions.php");
	include_once("job_Functions.php");
	
	$id = $_POST['id'];

	if(!getIsJobInvite($id))
	{
		$return_array = array(a => 0, b=> getMessage($id));
		echo json_encode($return_array);
	}
	
	else {
		$return_array = array(a => 1, b=> "Hello, <br> I would like to work on the following job:<br><br>" . getTitle(getMessageJOBID($id)));
		echo json_encode($return_array);
	}
?>