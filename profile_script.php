<?php
	
	session_start(); 
	include_once('lib/connect_db.php');
	include('profile_Functions.php');

	$username = getUserName($_SESSION['userID']);
	$email = getEmail($_SESSION['userID']);
	
	$zipcode = getUserLocation($_SESSION['userID']);
	if(empty($zipcode))
	{
		$zipcode = "set zipcode here";
	}
	
	$bio = getBio($_SESSION['userID']);
	if(empty($bio))
	{
		$bio = "set bio here";
	}

	$skill = getUserSkills($_SESSION['userID']);
	if(empty($skill))
	{
		$skill = "none";
	}

	echo json_encode(array("a" => $username, "b" => $email, "c" => $zipcode, "d" => $bio, "e" => $skill));

	//echo getUserName($_SESSION['userID']);
?>
