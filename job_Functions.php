<?php

include_once("query.php");	

// USERID of poster	6 character hex ID
// Title of job		String
// Description of job	String
// Payment for job	Float
// Due date for job	Date


function createJob($USERID, $title, $description, $payment, $dueDate, $skill, $zipcode){
	if(jobExists($USERID, $title, $description, $payment, $skill, $zipcode)){
		return false;
	}

	$sql = "insert into jobs (USERID, JobTitle, Description, Payment, DueDate, PostDate, Skills, Zipcode) values ($USERID, '$title', '$description', '$payment', '$dueDate', curdate(), '$skill', '$zipcode');";
        $res = query($sql);
	if($res==false){
		echo "Job Creation failed\n$title\n";
		return false;
	}
	
	else {
		return true;
	}


}


function jobExists($USERID, $title, $description, $payment, $skill, $zipcode){
	$sql = "select count(*) from jobs where USERID='$USERID' AND JobTitle='$title' AND Description='$description' AND Payment='$payment' AND Skills='$skill' AND Zipcode='$zipcode'";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
 	if($rs[0]>0){
		//echo "You have already created this job.";
		return true;
	}
	else{
		return false;
	}
}

function getNumJobsPosted($USERID){
	$sql = "select count(*) from jobs where USERID = $USERID;";
	$res = query($sql);
	return mysqli_result($res, 0);
}

function getNumJobsAccepted($Acceptor){
	$sql = "select count(*) from jobs where Acceptor = $Acceptor;";
	$res = query($sql);
	return mysqli_result($res, 0);
}


function deleteJob($JOBID){

	$sql = "delete from jobs where JOBID = '$JOBID'; ";
        $res = query($sql);
	if($res==false){
		echo "Job Delete failed\n";
	}
}

//Returns list of posted JOBIDs separated by #s
function getJOBIDs($USERID){
	$sql = "select JOBID from jobs where USERID = $JOBID;";
        $res = query($sql);
	while($rs = mysqli_fetch_array($res)){
		return $rs['JOBID'] . '#';
	}
}

//Returns list of accepted JOBIDs separated by #s
function getAcceptedJOBIDs($USERID){
	$sql = "select JOBID from jobs where Acceptor = '$USERID';";
        $res = query($sql);
	while($rs = mysqli_fetch_array($res)){
		return $rs['JOBID'] . '#';
	}
}

function getJOBID($title){
	$sql = "select JOBID from jobs where JobTitle = '$title';";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['JOBID'];
}

function setTitle($JOBID, $title){
	$sql = "update jobs set JobTitle = '$title' where JOBID = $JOBID; ";
        $res = query($sql);
}

function getTitle($JOBID){
	$sql = "select JobTitle from jobs where JOBID = $JOBID; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['JobTitle'];
}

function setDescription($JOBID, $desc){
	$sql = "update jobs set Description = '$desc' where JOBID = $JOBID; ";
        $res = query($sql);
}

function getDescription($JOBID){
	$sql = "select Description from jobs where JOBID = $JOBID; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['Description'];
}

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//Everything above this line is working well.
//the reason they rest may not be working is because you need
//to call the //setup() function at the top of each function
// and then delete the $connect variable from mysql_query()
//also check to make sure $rs['colummn'] has the ' ' marks



function setPayment($JOBID, $payment){
	$sql = "update jobs set Payment = '$payment' where JOBID = $JOBID; ";
        $res = query($sql);
}

function getPayment($JOBID){
	$sql = "select Payment from jobs where JOBID = $JOBID; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['Payment'];
}

function setPoster($JOBID, $USERID){
	$sql = "update jobs set USERID = $USERID where JOBID = $JOBID; ";
        $res = query($sql);
}

function getPoster($JOBID){
	$sql = "select USERID from jobs where JOBID = $JOBID; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['USERID'];
}

function setJobLocation($JOBID, $location){
	$sql = "update jobs set Zipcode = '$location' where JOBID = $JOBID; ";
        $res = query($sql);
}

function getJobLocation($JOBID){
	$sql = "select Zipcode from jobs where JOBID = $JOBID; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['Zipcode'];
}

function setPostDate($JOBID, $date){
	$sql = "update jobs set PostDate = '$date' where JOBID = $JOBID; ";
        $res = query($sql);
}

function getPostDate($JOBID){
	$sql = "select PostDate from jobs where JOBID = $JOBID; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['PostDate'];
}

function setDueDate($JOBID, $date){
	$sql = "update jobs set DueDate = '$date' where JOBID = $JOBID; ";
        $res = query($sql);
}

function getDueDate($JOBID){
	$sql = "select DueDate from jobs where JOBID = $JOBID; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['DueDate'];
}

function setIsCompleted($JOBID, $bool){
	$sql = "update jobs set Completed = $bool where JOBID = $JOBID; ";
        $res = query($sql);
}

function getIsCompleted($JOBID){
	$sql = "select Completed from jobs where JOBID = $JOBID; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['Completed'];
}

function setIsAccepted($JOBID, $bool){
	$sql = "update jobs set Accepted = $bool where JOBID = $JOBID; ";
        $res = query($sql);
}

function getIsAccepted($JOBID){
	$sql = "select Accepted from jobs where JOBID = $JOBID; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['Accepted'];
}

function setAcceptor($JOBID, $USERID){
	$sql = "update jobs set Acceptor = $USERID where JOBID = $JOBID; ";
	$res = query($sql);
	if(!getIsAccepted($JOBID)){
		setIsAccepted($JOBID, true);
	}
}

function getAcceptor($JOBID){
	$sql = "select Acceptor from jobs where JOBID = $JOBID; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['Acceptor'];
}

function setJobSkills($JOBID, $skills){
	$sql = "update jobs set Skills = '$skills' where JOBID = $JOBID; ";
        $res = query($sql);
}

function getJobSkills($JOBID){
	$sql = "select Skills from jobs where JOBID = $JOBID; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['Skills'];
}

function setIsConsumerApproved($JOBID, $bool){
	$sql = "update jobs set ConsumerApproved = $bool where JOBID = $JOBID; ";
        $res = query($sql);
}

function getIsConsumerApproved($JOBID){
	$sql = "select ConsumerApproved from jobs where JOBID = $JOBID; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['ConsumerApproved'];
}

function setIsProviderApproved($JOBID, $bool){
	$sql = "update jobs set ProviderApproved = $bool where JOBID = $JOBID; ";
        $res = query($sql);
}

function getIsProviderApproved($JOBID){
	$sql = "select ProviderApproved from jobs where JOBID = $JOBID; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['ProviderApproved'];
}

function setIsMoneyInEscrow($JOBID, $bool){
	$sql = "update jobs set MoneyInEscrow = $bool where JOBID = $JOBID; ";
        $res = query($sql);
}

function getIsMoneyInEscrow($JOBID){
	$sql = "select MoneyInEscrow from jobs where JOBID = $JOBID; ";
        $res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['MoneyInEscrow'];
}

function setIsMoneyTransferred($JOBID, $bool){
	$sql = "update jobs set MoneyTransferred = $bool where JOBID = $JOBID; ";
        $res = query($sql);
}

function getIsMoneyTransferred($JOBID){
	$sql = "select MoneyTransferred from jobs where JOBID = $JOBID; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['MoneyTransferred'];
}

?>