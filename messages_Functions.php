<?php

//Most functions will use the MESSAGEID variable to retrieve data.

include_once("query.php");


function createMessage($Sender, $Receiver, $ConvoTitle , $Message, $JobInvite /*Boolean*/, $NormMessage /*Boolean*/, $jobid){
	$sql = "insert into messages (Sender, Receiver, ConvoTitle, Message, JobInvite, NormMessage, DateSent, JOBID) values('$Sender', '$Receiver', '$ConvoTitle', '$Message', '$JobInvite', '$NormMessage', Curdate(), $jobid); ";
	$res = query($sql);
}

//include only Sender, Receiver, ConvoTitle
function getMESSAGEID($Sender, $Receiver, $ConvoTitle){
	$sql = "select MESSAGEID from messages where Sender = '$Sender' AND Receiver = '$Receiver' AND ConvoTitle = '$ConvoTitle';";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['MESSAGEID'];
	
}


//Will return one message's text by its MESSAGEID
function getMessage($MESSAGEID){
	$sql = "select Message from messages where MESSAGEID='$MESSAGEID'; ";
	$res = query($sql);	
	$rs = mysqli_fetch_array($res);
	return $rs['Message'];
}

function setMessage($MESSAGEID, $Message){
	$sql = "update messages set Messages='$Message' where MESSAGEID='$MESSAGEID'; ";
	$res = query($sql);
}

//Will return list of all MESSAGEIDs sent or received by a user
function getAllMessages($USERID){
	$sql = "select MESSAGEID from messages where Sender=$USERID or Receiver=$USERID; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	$temp = array();
	while($rs){
		array_push($temp, $rs['MESSAGEID']);
	}
	return $temp;
}

//Will return list of all MESSAGEIDs sent of a user
function getAllMessagesSent($USERID){
	$sql = "select MESSAGEID from messages where Sender=$USERID; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	$temp = array();
	while($rs){
		array_push($temp, $rs['MESSAGEID']);
	}
	return $temp;
}

//Will return list of all MESSAGEIDs received of a user
function getAllMessagesReceived($USERID){
	$sql = "select MESSAGEID from messages where Receiver=$USERID; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	$temp = array();
	while($rs){
		array_push($temp, $rs['MESSAGEID']);
	}
	return $temp;
}

function getDeletedMessagesSent($USERID){
	$sql = "select MESSAGEID from messages where Sender=$USERID and IsDeleted=true; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	$temp = array();
	while($rs){
		array_push($temp, $rs['MESSAGEID']);
	}
	return $temp;
}

function getDeletedMessagesReceived($USERID){
	$sql = "select MESSAGEID from messages where Receiver=$USERID and IsDeleted=true; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	$temp = array();
	while($rs){
		array_push($temp, $rs['MESSAGEID']);
	}
	return $temp;
}

function getFlaggedMessagesSent($USERID){
	$sql = "select MESSAGEID from messages where Sender=$USERID and IsFlagged=true; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	$temp = array();
	while($rs){
		array_push($temp, $rs['MESSAGEID']);
	}
	return $temp;
}

function getFlaggedMessagesReceived($USERID){
	$sql = "select MESSAGEID from messages where Receiver=$USERID and IsFlagged=true; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
        $temp = array();
        while($rs){
                array_push($temp, $rs['MESSAGEID']);
        }
        return $temp;
}

function getJobInviteMessagesSent($USERID){
	$sql = "select MESSAGEID from messages where Sender=$USERID and JobInvite=true; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
        $temp = array();
        while($rs){
                array_push($temp, $rs['MESSAGEID']);
        }
        return $temp;
}

function getJobInviteMessagesReceived($USERID){
	$sql = "select MESSAGEID from messages where Receiver=$USERID and JobInvite=true; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
        $temp = array();
        while($rs){
                array_push($temp, $rs['MESSAGEID']);
        }
        return $temp;
}


function getNormMessagesSent($USERID){
	$sql = "select MESSAGEID from messages where Sender=$USERID and NormMessage=true; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
        $temp = array();
        while($rs){
                array_push($temp, $rs['MESSAGEID']);
        }
        return $temp;
}

function getNormMessagesReceived($USERID){
	$sql = "select MESSAGEID where Receiver=$USERID and NormMessage=true; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
        $temp = array();
        while($rs){
                array_push($temp, $rs['MESSAGEID']);
        }
        return $temp;
}

function getConvoTitle($MESSAGEID){
	$sql = "select ConvoTitle from messages where MESSAGEID='$MESSAGEID'; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['ConvoTitle'];
}

function setConvoTitle($MESSAGEID, $title){
	$sql = "update messages set ConvoTitle='$title' where MESSAGEID='$MESSAGEID'; ";
	$res = query($sql);
}

function getSender($MESSAGEID){
	$sql = "select Sender from messages where MESSAGEID='$MESSAGEID'; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['Sender'];
}

function getReceiver($MESSAGEID){
	$sql = "select Receiver from messages where MESSAGEID='$MESSAGEID'; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['Receiver'];
}

function getIsJobInvite($MESSAGEID){
	$sql = "select JobInvite from messages where MESSAGEID='$MESSAGEID'; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['JobInvite'];
}

function getIsNormMessage($MESSAGEID){
	$sql = "select NormMessage from messages where MESSAGEID='$MESSAGEID'; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['NormMessage'];
}

function getDateSent($MESSAGEID){
	$sql = "select DateSent from messages where MESSAGEID='$MESSAGEID'; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['DateSent'];
}

function setIsDeleted($MESSAGEID, $bool){
	$sql = "update messages set IsDeleted=$bool where MESSAGEID='$MESSAGEID'; ";
	$res = query($sql);
}

function getIsDeleted($MESSAGEID){
	$sql = "select IsDeleted from messages where MESSAGEID='$MESSAGEID'; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['IsDeleted'];
}

function setIsFlagged($MESSAGEID, $bool){
	$sql = "update messages set IsFlagged=$bool where MESSAGEID='$MESSAGEID'; ";
	$res = query($sql);
}

function getIsFlagged($MESSAGEID){
	$sql = "select IsFlagged from messages where MESSAGEID='$MESSAGEID'; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['IsFlagged'];
}

function setReportNum($MESSAGEID){
	$sql = "update messages set ReportNum=ReportNum+1 where MESSAGEID='$MESSAGEID'; ";
	$res = query($sql);
}

function getReportNum($MESSAGEID){
	$sql = "select ReportNum from messages where MESSAGEID='$MESSAGEID'; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['ReportNum'];
}

function getMessageJOBID($MESSAGEID){
	$sql = "select JOBID from messages where MESSAGEID = '$MESSAGEID';";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['JOBID'];
}

function deleteMessages(){
	$sql = "delete from messages";
	$res = query($sql);
}

function getJOBIDfromMessageFunction($MESSAGEID){
	$sql = "select JOBID from messages where MESSAGEID='$MESSAGEID'; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['JOBID'];
}

?>

