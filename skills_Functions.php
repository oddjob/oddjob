<?php

// USERID of poster	6 character hex ID
// Title of job		String
// Description of job	String
// Payment for job	Float
// Due date for job	Date

include_once("query.php");

function createSkill($Name, $Category){
	
	if(skillNameExists($Name)){
		echo "Skill already exists\n";
		return;	
	}
	
	$sql = "insert into skills (Name, Category) values ('$Name', '$Category')";
	$res = query($sql);
	if($res == false){
		echo "Operation error";
	}
}

function deleteSkill($SKILLID){
	
	$sql = "delete from skills where SKILLID = '$SKILLID'; ";
	$res = query($sql);
}

//Returns a list of Skill Ids separated by #s
function getSKILLIDs(){
	
	$sql = "select SKILLID from skills;";
	$res = query($sql);
	$array = array();
	while($rs = mysqli_fetch_array($res)){
		array_push($array, $rs['SKILLID'].'#');
	}
	return $array;
}
//Return json array of skill names for front end
function getSkills(){
	
	$sql = "select Name from skills;";
	$res = query($sql);
	$array = array();
	while($rs = mysqli_fetch_array($res)){
		array_push($array, $rs['Name']);
	}
	return $array;
}

//Returns a list of Skill Ids separated by #s
function getSKILLIDsInCategory($Category){
	
	$sql = "select SKILLID from skills where Category = '$Category';";
	$res = query($sql);
	
	$array = array();
	while($rs = mysqli_fetch_array($res)){
		array_push($array, $rs['SKILLID'].'#');
	}
	return $array;
		
 
}

function getSKILLID($Name){
	
	$sql = "select SKILLID from skills where Name = '$Name'; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['SKILLID'];
}

function skillNameExists($Name){
	$sql = "select count(*) from skills where Name = '$Name';";
	$res = query();
	
	if (mysqli_result($res, 0) > 0){
		return true;
	}
	else{
		return false;
	}
}

function SKILLIDExists($Name){
	$SKILLID = getSKILLID($Name);
	$sql = "select count(*) from skills where SKILLID = $SKILLID;";
	$res = query();
	
	if (mysqli_result($res, 0) > 0){
		return true;
	}
	else{
		return false;
	}
}


function setName($SKILLID, $Name){
	
	$sql = "update skills set Name = '$Name' where SKILLID = '$SKILLID'; ";
	$res = query($sql);
}

function getName($SKILLID){
	
	$sql = "select Name from skills where SKILLID = '$SKILLID'; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['Name'];
}

function getCategory($SKILLID){
	
	$sql = "select Category from skills where SKILLID = '$SKILLID'; ";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['Category'];
}

function setCategory($SKILLID, $Category){
	
	$sql = "update skills set Category = '$Category' where SKILLID = '$SKILLID'; ";
	$res = query($sql);
}

//returns an array of Strings with the keys being integers starting at 0
function getCategories(){
	
	$sql = "select DISTINCT Category from skills;";
	$res = query($sql);
	$key = 0;
	$categories = [];
	while($rs = mysqli_fetch_array($res)){
		if($rs['Category'] != null){
			 $categories[$key++] = $rs['Category'];
		}
	}
	return $categories;
}


?>
