<?php

extract($_GET);
extract($_POST);
extract($_SERVER);

include "lib/connect_db.php";

function query($myQuery){
	global $connect;
        $res = mysqli_query($connect,$myQuery);
        if($res == false){
                echo "\nQuery failed:\t$myQuery\n";
        }
        else{
                //echo "Query Succeeded\n";
                return $res;
        }
}

function mysqli_result($res,$col=0){
	global $connect;  
        $numrows = mysqli_num_rows($res);
        $row = 0;
        if ($numrows && $row <= ($numrows-1) && $row >=0){
                mysqli_data_seek($res,$row);
                $resrow = (is_numeric($col)) ? mysqli_fetch_row($res) : mysqli_fetch_assoc($res);
                if (isset($resrow[$col])){
                        return $resrow[$col];
                }
        }
        return false;
}

?>
