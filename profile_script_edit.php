<?php
	session_start();
	include_once('lib/connect_db.php');
	//include('lib/session.php');
	include('profile_Functions.php');

	$username = $_POST['username'];	
	$email = $_POST['email'];
	$zipcode = $_POST['zipcode'];
	$bio = $_POST['bio'];
	$skill = $_POST['skill']; 

	if($username != getUserName($_SESSION['userID']) && !empty($username))
	{
		if(!usernameExists($username))
		{
			setUsername($_SESSION['userID'], $username);
		}
	}

	if($email != getEmail($_SESSION['userID']) && !empty($email))
	{
		if(!emailExists($email))
		{
			setEmail($_SESSION['userID'], $email); 
		}
	}
	
	if ($zipcode != getUserLocation($_SESSION['userID']) && !empty($zipcode))
	{
		setUserLocation($_SESSION['userID'], $zipcode);
	}

	if($bio != getBio($_SESSION['bio']) && !empty($bio))
	{
		setBio($_SESSION['userID'], $bio);
	}
	
	if($skill != getUserSkills($_SESSION['userID']) && !empty($skill))
	{
		setUserSkills($_SESSION['userID'], $skill);
	}

?> 