<?php

include_once("query.php");

//Most functions will use the REPORTID variable to retrieve data.


function getREPORTID($TargetID, $InitiatorID, $ProcessorID){
	$sql = "select REPORTID from reports where TargetID = '$TargetID' AND InitiatorID = '$InitiatorID' AND ProcessorID = '$ProcessorID'";
	$res = query($sql);
	$rs = mysqli_fetch_array($res);
	return $rs['REPORTID'];
}


function createReport($Priority /* 0 through 10, 10 = worse */, $History, $InitiatorID, $TargetID, $ProcessorID /*Can be null*/){
	$sql = "insert into reports (Priority, History, InitiatorID, TargetID, ProcessorID) values ($Priority, '$History', '$InitiatorID', '$TargetID', '$ProcessorID');";
	query($sql);
}

//Will return one report by its ReportID
function getHistory($REPORTID){
	$sql = "select History from reports where REPORTID = '$REPORTID'";
	$res = query($sql);
	$rs =  mysqli_fetch_array($res);
	return $rs['History'];
}

function setHistory($REPORTID, $History){
	$sql = "update reports set History = '$History' where REPORTID = '$REPORTID'";
	query($sql);
}

function appendToHistory($REPORTID, $Entry){
	$newHistory = getHistory($REPORTID) . "\n" . $Entry;
	setHistory($REPORTID, $newHistory);
}

//We should use this function by default
function addHistoryEntry($REPORTID, $AuthorID, $Entry){
	include "profile_Functions.php";
	$newEntry = date('l jS \of F Y h:i:s A') . "\n" . "Author: " . getUserName($AuthorID) . "\n" . $Entry;

	appendToHistory($newEntry);
 
}

function getReporterID($REPORTID){
	$sql = "select InitiatorID from reports where REPORTID = $REPORTID";
	$res = query($sql);
	return mysqli_result($res, 0);
}

function getReportTargetID($REPORTID){
	$sql = "select TargetID from reports where REPORTID = $REPORTID";
	$res = query($sql);
	return mysqli_result($res, 0);
}

function getProcessorID($REPORTID){
	$sql = "select ProcessorID from reports where REPORTID = $REPORTID";
	$res = query($sql);
	return mysqli_result($res, 0);
	
}

function setIsResolved($REPORTID, $bool){
	$sql = "update reports set IsResolved = $bool where REPORTID = $REPORTID";
	query($sql);
	
}

function getIsResolved($REPORTID){
	$sql = "select IsResolved from reports where REPORTID = $REPORTID";
	$res = query($sql);
	return mysqli_result($res, 0);
	
}

function getIsReviewed($REPORTID){
	$sql = "select IsReviewed from reports where REPORTID = $REPORTID";
	$res = query($sql);
	return mysqli_result($res, 0);
	
}

function setIsReviewed($REPORTID, $bool){
	$sql = "update reports set IsReviewed = $bool where REPORTID = $REPORTID";
	query($sql);
	
}

function getIsInProgress($REPORTID){
	$sql = "select InProgress from reports where REPORTID = $REPORTID";
	$res = query($sql);
	return mysqli_result($res, 0);
	
}

function setIsInProgress($REPORTID, $bool){
	$sql = "update reports set InProgress = $bool where REPORTID = $REPORTID";
	query($sql);
	
}



//Will return list of REPORTIDs initiated by a user
function getReportsInitiated($InitiatorID){
	$sql = "select REPORTID from reports where InitiatorID = $InitiatorID";
	$res = query($sql);
	$array = array();
        while($rs = mysqli_fetch_array($res)){
                array_push($array, $rs['REPORTID']);
        }
        return $array;	
}

//Will Return list of REPORTIDs targeted at a user
function getReportsTarget($TargetID){
	$sql = "select REPORTID from reports where TargetID = $TargetID";
	$res = query($sql);
	$array = array();
        while($rs = mysqli_fetch_array($res)){
                array_push($array, $rs['REPORTID']);
        }
        return $array;

}

function deleteReports(){
	$sql = "delete from reports";
	query($sql);
}

?>
