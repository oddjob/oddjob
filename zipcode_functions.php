<?php

	include_once('query.php');
	
	/*
		This function utilizes the Google geocoding API to get the distance between two zipcodes. 

		As there is no equation to simply find the distance bewteen zipcodes, we use the geocoding api
		to get the latitude and longitude for each zip code.
		
		If it is not already added to an sql table, we add the zipcode with its latitude and longitude, so 
		we can simply lookup the table instead of making many api requests. 

		We then find the latitude and longitude using the Haversine formula, which calculates the distance 
		in kilometers. Info on the Haversine formula: http://rosettacode.org/wiki/Haversine_formula

		Geocoding API documentation: https://developers.google.com/maps/documentation/geocoding/intro
	*/ 

	function getDistanceBetween($zipcode1, $zipcode2)
	{
		//check to see if the zipcode is already in table
		$sql = "select Latitude, Longitude from zipcodes where Zipcode=$zipcode1 limit 1;";
		$sql2 = "select Latitude, Longitude from zipcodes where Zipcode=$zipcode2 limit 1;";

        	$res1 = query($sql);
		$res2 = query($sql2);

		$value = mysqli_fetch_object($res1);
		$value2 = mysqli_fetch_object($res2);
		
		$key = 'AIzaSyD_Md_1KPMZWjQbzYUR2rowyLKq5KxEnPg';

		if(!empty($value)){
			$lat1 = $value->Latitude;
			$lng1 = $value->Longitude;
		}
		else{
			// if its not in table, add it to table
			$url1 = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $zipcode1 . '&key=' . $key;
			$json1 = file_get_contents($url1);
			$json_array1 = json_decode($json1, true);

			$lat1 = $json_array1['results'][0]['geometry']['location']['lat'];
			$lng1 = $json_array1['results'][0]['geometry']['location']['lng'];
		
			$sql = "insert into zipcodes (Zipcode, Latitude, Longitude) values ($zipcode1, $lat1, $lng1);";
			query($sql); 

		}
		// check to see if it is in table
		if(!empty($value2))
		{
			$lat2 = $value2->Latitude;
			$lng2 = $value2->Longitude;
		}
		else {
			// if its not in table, add it to table
			$url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $zipcode2 . '&key=' . $key;
			$json = file_get_contents($url);
			$json_array = json_decode($json, true);

			$lat2 = $json_array['results'][0]['geometry']['location']['lat'];
			$lng2 = $json_array['results'][0]['geometry']['location']['lng'];
		
			$sql = "insert into zipcodes (Zipcode, Latitude, Longitude) values ($zipcode2, $lat2, $lng2);";
			query($sql); 
			
		}


		//haversine formula 
		$earthRadius = 6371; 
		
		$deltaLat = deg2rad($lat2 - $lat1);
		$deltaLng = deg2rad($lng2 - $lng1);

		$angle = sin($deltaLat/2) * sin($deltaLat/2) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * sin($deltaLng) * sin($deltaLng/2);
		$c = 2 * asin(sqrt($angle));

		// multiply by 0.621371 to get distance in miles
		$distance = $earthRadius * $c * 0.621371;

		
		return round($distance, 1) ; 
	}

	/* 
		The purpose of this function is to get all zipcodes in a certain radius of a zipcode.
		
		Since we have a free account, we the max radius is 30 and the most results we can get is 5. 

	*/

	function getZipCodesInRadius($zipcode, $radius)
	{
		$url = "http://api.geonames.org/findNearbyPostalCodesJSON?postalcode=" . $zipcode . "&radius=" . $radius . "&username=oddjob";

		$json = file_get_contents($url);
		$json_array = json_decode($json, true);
		
		//put all results in an array
		$return_array = array();
		for($i = 0; $i < count($json_array['postalCodes']); $i++)
		{
			array_push($return_array, $json_array['postalCodes'][$i]['postalCode']);
		}
		
		return $return_array; 

	}

?>
