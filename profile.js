$(document).ready(function()
{
	$.post("profile_script.php", function(data){
		$('#username').text(data);
		$('#username2').text(data);
	})
	.fail(function(){
		alert('Error: Could not load username');
	})

	$.post("bio_script.php", function(data){
		$('#bio').text(data);
	})
	.fail(function(){
		alert('Error: Could not load user bio');
	});

	$.post("skills_script.php", function(data){
		$('#skills').text(data);
	})
	.fail(function(){
		alert('Error: Could not load user skills');
	});

	$.post("dob_script.php", function(data){
		$('#dob').text(data);
	})
	.fail(function(){
		alert('Error: Could not load user\'s DOB');
	});
});

function editProfile() {
	var popup = document.createElement("div");
	popup.setAttribute('class', 'popup');

	popup.innerHTML = ""
	+"<h3>Date of Birth:<h3>"
	+"<div class=\"field\" id=\"puDOB\" contentEditable=\"true\"></div>"


	+"<h3>Skills:</h3>"
	+"<div class=\"field\" id=\"puSKILLS\" contentEditable=\"true\"></div>"


	+"<h3>Bio:</h3>"
	+"<div class=\"field\" id=\"puBIO\" contentEditable=\"true\"></div>"


	+"<h3>zip code:</h3>"
	+"<div class=\"field\" id=\"puzipcode\" contentEditable=\"true\"></div>"



	+"<button class=\"button\" id=\"puAPPLY\" onclick=\"apply();\">Apply</button>"
	+"<button class=\"button\" id=\"puCANCEL\" onclick=\"cancel();\">Cancel</button>"

	var p = document.getElementById("popupForm")
	p.appendChild(popup);
}

function apply() {
	//Need to update user DOB, SKILLS, BIO
	var dob = $('#puDOB').text();
	var skl = $('#puSKILLS').text();
	var bio = $('#puBIO').text();

	$.post("update_profile.php", {DOB: dob, SKILLS: skl, BIO: bio})
	.fail(function(){
		alert("Error: Could not upload changes to profile.");
	});

	cancel();
	window.location='profile.html';		
}

function cancel() {
	var p = document.getElementById("popupForm");
	p.innerHTML = "";
}
